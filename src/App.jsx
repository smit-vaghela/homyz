import Header from "./components/Header/Header";
import Hero from "./components/Hero/Hero";
import './App.css'
import Companies from "./components/Companies/Companies";
import Residencies from "./components/Residencies/Rescidencies";
import Value from "./components/Value/Value";
import Contact from "./components/Contacts/Contact";
import Getstarted from "./components/GetStarted/Getstarted";
import Footer from "./components/Footer/Footer";

function App() {
  return (
    <div className="App">
      <div>
       <div className="white-gradient"></div> 
      <Header/>
      <Hero/>
      </div>
      <Companies/>
      <Residencies/>
      <Value/>
      <Contact/>
      <Getstarted/>
      <Footer/>
    </div>
  );
}

export default App;
