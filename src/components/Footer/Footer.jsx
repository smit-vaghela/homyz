import React from "react";
import './Footer.css'

const Footer = () => {
    return(
        <section className="f-wrapper">
            <div className="paddings innerWidth flexCenter f-container">
                {/* left */}
                <div className="flexColStart f-left">
                    <img src="./logo2.png" alt="" width={120}/>
                    <span className="secondayText">Our Vision and Mission <br/> Is to serve people for their dreams</span>
                </div>   

                 {/* right  */}
                 <div className="flexColStart f-right">
                    <span className="primaryText">Office</span>
                    <span className="secondaryText">Two Oaks, Dublin</span>

                    <div className="flexCenter f-menu">
                        <span>Propery</span>
                        <span>Services</span>
                        <span>Product</span>
                        <span>About Us</span>
                    </div>
                 </div>
            </div>
        </section>
    )
}

export default Footer 