import React from "react";
import './Hero.css';
import {HiLocationMarker} from 'react-icons/hi';
import CountUp from "react-countup";
import {motion} from 'framer-motion'


const Hero = () => {
    return (
        <section className="hero-wrapper">
           <div className="paddings innerWidth flexCenter hero-container">

                {/* Left Side */}
                <div className="flexColStart hero-left">
                    <div className="hero-title">
                        <div className="hovering-circle"></div>
                        <motion.h1
                        initial={{y: "2rem", opacity: "0"}}
                        animate={{y: "0", opacity: "1"}}
                        transition={{
                            duration: 2,
                            type: "keyframes"
                        }}
                        > 
                            Discover <br/> More Suitable <br/> Property
                        </motion.h1>
                    </div>

                    <div className="flexColStart hero-decription">
                        <span className="secondaryText">
                            Find a variety of properties that suit you very easily
                        </span>
                        <span className="secondaryText">
                            Forget all difficulties in finding a residence for you    
                        </span>
                    </div>

                    <div className="search-bar">
                        <HiLocationMarker color="var(--blue)" size={25}/>
                        <input type='text' placeholder="Search by location"/>
                        <button className="button">Search</button>
                    </div>

                    <div className="flexCenter hero-stats">
                        <div className="flexColCenter stat">
                            <span>
                                <CountUp start={800} end={1290} duration={3}/>
                                <span>+</span>
                            </span>
                            <span className="secondaryText">Premium Products</span>
                        </div>

                        <div className="flexColCenter stat">
                            <span>
                                <CountUp start={1000} end={2000} duration={3}/>
                                <span>+</span>
                            </span>
                            <span className="secondaryText">Happy Customers</span>
                        </div>

                        <div className="flexColCenter stat">
                            <span>
                                <CountUp start={89} end={170} duration={3}/>
                                <span>+</span>
                            </span>
                            <span className="secondaryText">Awards Wins</span>
                        </div>
                    </div>
                </div>

                {/* Right Side */}
                <div className="flexCenter hero-right">
                    <motion.div className="image-container"
                    initial={{x: "10rem", opacity: "0"}}
                    animate={{x: "0", opacity: "1"}}
                    transition={{
                        duration: 2,
                        type: "keyframes"
                    }}
                    >
                        <img src="./hero-image.png" alt="" />
                    </motion.div>
                </div>
           </div>
        </section>
    )
}

export default Hero