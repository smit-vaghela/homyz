import React from "react";
import './Getstarted.css'

const Getstarted = () => {
    return(
        <section className="g-wrapper">
            <div className="paddings innerWidth g-container">
                <div className="flexColCenter inner-container">
                    <span className="primaryText">Get started with Us</span>
                    <span className="secondaryText">Get a change to build a dream house or find one for yourself
                        <br />
                        Eventually you are going to live in your dream house.
                    </span>

                    <button className="button">
                        <a href="mailto:smitvaghela33@gmail.com">Get Started</a>
                    </button>
                </div>
            </div>
        </section>
    )
}

export default Getstarted