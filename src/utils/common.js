export const sliderSettings = {
    slidesPerView: 1,
    spaceBetween: 30,
    breakpoints:{
        480:{
            slidePerView: 1
        },
        600:{
            slidePerView: 2
        },
        750:{
            slidePerView: 3
        },
        1100:{
            slidePerView: 4
        },
    }
}